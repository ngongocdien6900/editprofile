import editReducer from "./edit";
import { combineReducers } from 'redux';

const rootReducer = combineReducers({
    edit: editReducer,
});

export default rootReducer;  

