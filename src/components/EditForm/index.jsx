import React, { useState } from "react";
import PropTypes from "prop-types";
import "./style.scss";

EditForm.propTypes = {
    onSubmit: PropTypes.func,
};

EditForm.defaultProps = {
    onSubmit: null
}

function EditForm(props) {

    const {onSubmit} = props;

    const [valueForm, setValueForm] = useState({
        storeName: '',
        storeAddress: '',
        storeDistrict: '',
        storeCity: '',
        storePhone: '',
        companyName: '',
        companyAddress: '',
        companyDistrict: '',
        companyCity: '',
        companyMST: '',
    })

    const [picture, setPicture] = useState(null);
    const [imgData, setImgData] = useState(null);
    const ref = React.useRef();

    const onChangePicture = e => {
      if (e.target.files[0]) {
        console.log("picture: ", e.target.files);
        setPicture(e.target.files[0]);
        const reader = new FileReader();
        reader.addEventListener("load", () => {
          setImgData(reader.result);
        });
        reader.readAsDataURL(e.target.files[0]);
      }
    };
    
    const handleValueChange = (e) => {
        
        let {name, value} = e.target;
        setValueForm({
            ...valueForm,
            [name]: value
        })
    }

    const handleFormSubmit = e => {
        e.preventDefault();

        for(let key in valueForm) {
            if(valueForm[key] === '') {
                return;
            }
        }

        onSubmit(valueForm)
        resetValueForm();
    }


    const resetValueForm = () => {
      setValueForm({
        storeName: '',
        storeAddress: '',
        storeDistrict: '',
        storeCity: '',
        storePhone: '',
        companyName: '',
        companyAddress: '',
        companyDistrict: '',
        companyCity: '',
        companyMST: '',
      })
      ref.current.value = "";
      setImgData(null)
    }

    const handleCancelClick = () => {
      resetValueForm();
    }

  return (
    <div className="edit-form">
      <div className="edit-form__left"> 

        <div className="image__preview">
          <img src={imgData} alt=""/>
        </div>

        <input type="file" ref={ref} onChange={onChangePicture}/>

      </div>
      <div className="edit-form__right">
        <h3 className="edit-form__title">BASIC INFO</h3>
        <form onSubmit={handleFormSubmit}>
          <div className="form-group">
            <label htmlFor="storeName">Store Name</label>
            <input
              type="text"
              className="form-control"
              id="storeName"
              aria-describedby="emailHelp"
              placeholder="Store name"
              onChange={handleValueChange}
              name="storeName"
              value={valueForm.storeName}
            />
          </div>

          <div className="form-group">
            <label htmlFor="storeAddress">Store Address</label>

            <div className="form-row">
              <div className="col-md-6 mb-3">
                <input
                  type="text"
                  className="form-control"
                  id="storeAddress"
                  placeholder="City"
                  onChange={handleValueChange}
                  value={valueForm.storeAddress}
                  name="storeAddress"
                />
              </div>
              <div className="col-md-3 mb-3">
                <select 
                id="inputState" 
                className="form-control" 
                onChange={handleValueChange} 
                value={valueForm.storeDistrict}
                name="storeDistrict"
                >
                  <option defaultValue>District</option>
                  <option value="Quận 11">Q11</option>
                  <option value="Quận 12">Q12</option>
                </select>
              </div>
              <div className="col-md-3 mb-3">
                <select id="inputState" className="form-control" 
                onChange={handleValueChange} 
                value={valueForm.storeCity}
                name="storeCity"
                >
                  <option defaultValue>City</option>
                  <option value="Hà Nội">HN</option>
                  <option value="Gia Lai">GL</option>
                </select>
              </div>
            </div>
          </div>

          <div className="form-group">
            <label htmlFor="phone">Phone</label>
            <input
              type="text"
              className="form-control"
              id="phone"
              aria-describedby="emailHelp"
              placeholder="Phone number"
              onChange={handleValueChange}
              value={valueForm.storePhone}
              name="storePhone"
            />
          </div>

          <h3 className="edit-form__title">RED INVOICE INFO</h3>

          <div className="form-group">
            <label htmlFor="storeName">Company Name</label>
            <input
              type="text"
              className="form-control"
              id="storeName"
              aria-describedby="emailHelp"
              placeholder="Company name"
              onChange={handleValueChange}
              value={valueForm.companyName}
              name="companyName"
            />
          </div>

          <div className="form-group">
            <label htmlFor="storeAddress">Company Address</label>

            <div className="form-row">
              <div className="col-md-6 mb-3">
                <input
                  type="text"
                  className="form-control"
                  id="storeAddress"
                  placeholder="City"
                  onChange={handleValueChange}
                  value={valueForm.companyAddress}
                  name="companyAddress"
                />
              </div>
              <div className="col-md-3 mb-3">
                <select id="inputState" className="form-control" 
                onChange={handleValueChange} 
                value={valueForm.companyDistrict}
                name="companyDistrict"
                >
                  <option defaultValue>District</option>
                  <option value="Quận 11">Q11</option>
                  <option value="Quận 12">Q12</option>
                </select>
              </div>
              <div className="col-md-3 mb-3">
                <select id="inputState" className="form-control" 
                onChange={handleValueChange} 
                value={valueForm.companyCity}
                name="companyCity"
                >
                  <option defaultValue>City</option>
                  <option value="Hà Nội">HN</option>
                  <option value="Gia Lai">GL</option>
                </select>
              </div>
            </div>
          </div>

          <div className="form-group">
            <label htmlFor="phone">MST</label>
            <input
              type="text"
              className="form-control"
              id="phone"
              aria-describedby="emailHelp"
              placeholder="Phone number"
              onChange={handleValueChange}
              value={valueForm.companyMST}
              name="companyMST"
            />
          </div>
          <button className="btn btn-success edit-form__btn">Save</button>
          <button className="btn btn-light edit-form__btn" onClick={handleCancelClick}>Cancel</button>
        </form>
      </div>
    </div>
  );
}

export default EditForm;
