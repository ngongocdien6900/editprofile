import { useDispatch, useSelector } from "react-redux";
import { editProfile } from "./actions/edit";
import "./App.scss";
import EditForm from "./components/EditForm";

function App() {

  const dispatch = useDispatch();
  const isLoading = useSelector(state => state.edit.isLoading);

  const handleFormSubmit = () => {
    dispatch(editProfile());
  }

  return (
    <div className="edit">
      <div className="container">
        {isLoading && <img src="https://s0.gifyu.com/images/loading81d3e.gif" alt="" className="edit__loading"/> }
        <EditForm onSubmit={handleFormSubmit}/>
      </div>
    </div>
  );
}

export default App;
